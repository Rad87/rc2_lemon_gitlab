#ifndef SERVER_MANAGER_sqliteInt_h__
#define SERVER_MANAGER_sqliteInt_h__


/*
** Integers of known sizes.  These typedefs might change for architectures
** where the sizes very.  Preprocessor macros are available so that the
** types can be conveniently redefined at compile-type.  Like this:
**
**         cc '-DUINTPTR_TYPE=long long int' ...
*/
#ifndef UINT32_TYPE
# ifdef HAVE_UINT32_T
#  define UINT32_TYPE uint32_t
# else
#  define UINT32_TYPE unsigned int
# endif
#endif
#ifndef UINT16_TYPE
# ifdef HAVE_UINT16_T
#  define UINT16_TYPE uint16_t
# else
#  define UINT16_TYPE unsigned short int
# endif
#endif
#ifndef INT16_TYPE
# ifdef HAVE_INT16_T
#  define INT16_TYPE int16_t
# else
#  define INT16_TYPE short int
# endif
#endif
#ifndef UINT8_TYPE
# ifdef HAVE_UINT8_T
#  define UINT8_TYPE uint8_t
# else
#  define UINT8_TYPE unsigned char
# endif
#endif
#ifndef INT8_TYPE
# ifdef HAVE_INT8_T
#  define INT8_TYPE int8_t
# else
#  define INT8_TYPE signed char
# endif
#endif
#ifndef LONGDOUBLE_TYPE
# define LONGDOUBLE_TYPE long double
#endif

typedef long long int i64;          /* 8-byte signed integer */
typedef long long unsigned int u64;         /* 8-byte unsigned integer */
typedef UINT32_TYPE u32;           /* 4-byte unsigned integer */
typedef UINT16_TYPE u16;           /* 2-byte unsigned integer */
typedef INT16_TYPE i16;            /* 2-byte signed integer */
typedef UINT8_TYPE u8;             /* 1-byte unsigned integer */
typedef INT8_TYPE i8;              /* 1-byte signed integer */
/*
** A sort order can be either ASC or DESC.
*/
#define SQLITE_SO_ASC       0  /* Sort in ascending order */
#define SQLITE_SO_DESC      1  /* Sort in ascending order */

/*
** Size of the column cache
*/
#ifndef SQLITE_N_COLCACHE
# define SQLITE_N_COLCACHE 10
#endif

/*
** Allowed values for Select.selFlags.  The "SF" prefix stands for
** "Select Flag".
*/
#define SF_Distinct        0x0001  /* Output should be DISTINCT */
#define SF_Resolved        0x0002  /* Identifiers have been resolved */
#define SF_Aggregate       0x0004  /* Contains aggregate functions */
#define SF_UsesEphemeral   0x0008  /* Uses the OpenEphemeral opcode */
#define SF_Expanded        0x0010  /* sqlite3SelectExpand() called on this */
#define SF_HasTypeInfo     0x0020  /* FROM subqueries have Table metadata */
#define SF_UseSorter       0x0040  /* Sort using a sorter */
#define SF_Values          0x0080  /* Synthesized from VALUES clause */
#define SF_Materialize     0x0100  /* Force materialization of views */
#define SF_NestedFrom      0x0200  /* Part of a parenthesized FROM clause */

/*
** Permitted values of the SrcList.a.jointype field
*/
#define JT_INNER     0x0001    /* Any kind of inner or cross join */
#define JT_CROSS     0x0002    /* Explicit use of the CROSS keyword */
#define JT_NATURAL   0x0004    /* True for a "natural" join */
#define JT_LEFT      0x0008    /* Left outer join */
#define JT_RIGHT     0x0010    /* Right outer join */
#define JT_OUTER     0x0020    /* The "OUTER" keyword is present */
#define JT_ERROR     0x0040    /* unknown or unsupported join type */

#define SRT_Output       5  /* Output each row of result */
#define SRT_Mem          6  /* Store result in a memory cell */
#define SRT_Set          7  /* Store results as keys in an index */
#define SRT_Table        8  /* Store result as data with an automatic rowid */
#define SRT_EphemTab     9  /* Create transient tab and store like SRT_Table */
#define SRT_Coroutine   10  /* Generate a single row of result */


/*
** The maximum number of attached databases.  This must be between 0
** and 62.  The upper bound on 62 is because a 64-bit integer bitmap
** is used internally to track attached databases.
*/
#ifndef SQLITE_MAX_ATTACHED
# define SQLITE_MAX_ATTACHED 10
#endif

#define sqlite3StrNICmp sqlite3_strnicmp
/* Convenient short-hand */
#define UpperToLower sqlite3UpperToLower
#define ArraySize(X)    ((int)(sizeof(X)/sizeof(X[0])))

#define  SQLITE_ASCII
#ifdef SQLITE_ASCII
# define charMap(X) sqlite3UpperToLower[(unsigned char)X]
#endif






static const unsigned char sqlite3UpperToLower[] = {
#ifdef SQLITE_ASCII
    0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, 16, 17,
    18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,
    36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53,
    54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 97, 98, 99,100,101,102,103,
    104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,
    122, 91, 92, 93, 94, 95, 96, 97, 98, 99,100,101,102,103,104,105,106,107,
    108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,
    126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,
    144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,
    162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,
    180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,
    198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,
    216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,
    234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,
    252,253,254,255
#endif
#ifdef SQLITE_EBCDIC
    0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15, /* 0x */
    16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, /* 1x */
    32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, /* 2x */
    48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, /* 3x */
    64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, /* 4x */
    80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, /* 5x */
    96, 97, 66, 67, 68, 69, 70, 71, 72, 73,106,107,108,109,110,111, /* 6x */
    112, 81, 82, 83, 84, 85, 86, 87, 88, 89,122,123,124,125,126,127, /* 7x */
    128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143, /* 8x */
    144,145,146,147,148,149,150,151,152,153,154,155,156,157,156,159, /* 9x */
    160,161,162,163,164,165,166,167,168,169,170,171,140,141,142,175, /* Ax */
    176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191, /* Bx */
    192,129,130,131,132,133,134,135,136,137,202,203,204,205,206,207, /* Cx */
    208,145,146,147,148,149,150,151,152,153,218,219,220,221,222,223, /* Dx */
    224,225,162,163,164,165,166,167,168,169,232,203,204,205,206,207, /* Ex */
    239,240,241,242,243,244,245,246,247,248,249,219,220,221,222,255, /* Fx */
#endif
};

/*
** Each token coming out of the lexer is an instance of
** this structure.  Tokens are also used as part of an expression.
**
** Note if Token.z==0 then Token.dyn and Token.n are undefined and
** may contain random values.  Do not make any assumptions about Token.dyn
** and Token.n when Token.z==0.
*/
struct Token {
    const char *z;     /* Text of the token.  Not NULL-terminated! */
    unsigned int n;    /* Number of characters in this token */
};
typedef struct Token Token;
typedef struct SelectDest SelectDest;

/*
** An SQL parser context.  A copy of this structure is passed through
** the parser and down into all the parser action routine in order to
** carry around information that is global to the entire parse.
**
** The structure is divided into two parts.  When the parser and code
** generate call themselves recursively, the first part of the structure
** is constant but the second part is reset at the beginning and end of
** each recursion.
**
** The nTableLock and aTableLock variables are only used if the shared-cache 
** feature is enabled (if sqlite3Tsd()->useSharedData is true). They are
** used to store the set of table-locks required by the statement being
** compiled. Function sqlite3TableLock() is used to add entries to the
** list.
*/
struct Parse;
typedef struct Parse Parse;


/*
** The following are used as the second parameter to sqlite3Savepoint(),
** and as the P1 argument to the OP_Savepoint instruction.
*/
#define SAVEPOINT_BEGIN      0
#define SAVEPOINT_RELEASE    1
#define SAVEPOINT_ROLLBACK   2
/*
** The following are the meanings of bits in the Expr.flags2 field.
*/
#define EP2_MallocedToken  0x0001  /* Need to sqlite3DbFree() Expr.zToken */
#define EP2_Irreducible    0x0002  /* Cannot EXPRDUP_REDUCE this Expr */

void sqlite3Parser(void*, int, Token, Parse*);

struct Parse {
  void * db;
  char *zErrMsg;       /* An error message */
  int rc;              /* Return code from execution */
  u8 colNamesSet;      /* TRUE after OP_ColumnName has been issued to pVdbe */
  u8 checkSchema;      /* Causes schema cookie check after an error */
  u8 nested;           /* Number of nested calls to the parser/code generator */
  u8 nTempReg;         /* Number of temporary registers in aTempReg[] */
  u8 nTempInUse;       /* Number of aTempReg[] currently checked out */
  u8 nColCache;        /* Number of entries in aColCache[] */
  u8 iColCache;        /* Next entry in aColCache[] to replace */
  u8 isMultiWrite;     /* True if statement may modify/insert multiple rows */
  u8 mayAbort;         /* True if statement may throw an ABORT exception */
  int aTempReg[8];     /* Holding area for temporary registers */
  int nRangeReg;       /* Size of the temporary register block */
  int iRangeReg;       /* First register in temporary register block */
  int nErr;            /* Number of errors seen */
  int nTab;            /* Number of previously allocated VDBE cursors */
  int nMem;            /* Number of memory cells used so far */
  int nSet;            /* Number of sets used so far */
  int nOnce;           /* Number of OP_Once instructions so far */
  int ckBase;          /* Base register of data during check constraints */
  int iCacheLevel;     /* ColCache valid when aColCache[].iLevel<=iCacheLevel */
  int iCacheCnt;       /* Counter used to generate aColCache[].lru values */
  struct yColCache {
    int iTable;           /* Table cursor number */
    int iColumn;          /* Table column number */
    u8 tempReg;           /* iReg is a temp register that needs to be freed */
    int iLevel;           /* Nesting level */
    int iReg;             /* Reg with value of this column. 0 means none. */
    int lru;              /* Least recently used entry has the smallest value */
  } aColCache[SQLITE_N_COLCACHE];  /* One for each column cache entry */
  int cookieGoto;      /* Address of OP_Goto to cookie verifier subroutine */
  int cookieValue[SQLITE_MAX_ATTACHED+2];  /* Values of cookies to verify */
  int regRowid;        /* Register holding rowid of CREATE TABLE entry */
  int regRoot;         /* Register holding root page number for new objects */
  int nMaxArg;         /* Max args passed to user function by sub-program */
  Token constraintName;/* Name of the constraint currently being parsed */
#ifndef SQLITE_OMIT_SHARED_CACHE
  int nTableLock;        /* Number of locks in aTableLock */
  //TableLock *aTableLock; /* Required table locks for shared-cache mode */
#endif
  //AutoincInfo *pAinc;  /* Information about AUTOINCREMENT counters */

  /* Information used while coding trigger programs. */
  Parse *pToplevel;    /* Parse structure for main program (or NULL) */
  //Table *pTriggerTab;  /* Table triggers are being coded for */
  double nQueryLoop;   /* Estimated number of iterations of a query */
  u32 oldmask;         /* Mask of old.* columns referenced */
  u32 newmask;         /* Mask of new.* columns referenced */
  u8 eTriggerOp;       /* TK_UPDATE, TK_INSERT or TK_DELETE */
  u8 eOrconf;          /* Default ON CONFLICT policy for trigger steps */
  u8 disableTriggers;  /* True to disable triggers */

  /* Above is constant between recursions.  Below is reset before and after
  ** each recursion */

  int nVar;                 /* Number of '?' variables seen in the SQL so far */
  int nzVar;                /* Number of available slots in azVar[] */
  u8 explain;               /* True if the EXPLAIN flag is found on the query */
#ifndef SQLITE_OMIT_VIRTUALTABLE
  u8 declareVtab;           /* True if inside sqlite3_declare_vtab() */
  int nVtabLock;            /* Number of virtual tables to lock */
#endif
  int nAlias;               /* Number of aliased result set columns */
  int nHeight;              /* Expression tree height of current sub-select */
#ifndef SQLITE_OMIT_EXPLAIN
  int iSelectId;            /* ID of current select for EXPLAIN output */
  int iNextSelectId;        /* Next available select ID for EXPLAIN output */
#endif
  char **azVar;             /* Pointers to names of parameters */
  //Vdbe *pReprepare;         /* VM being reprepared (sqlite3Reprepare()) */
  int *aAlias;              /* Register used to hold aliased result */
  const char *zTail;        /* All SQL text past the last semicolon parsed */
  //Table *pNewTable;         /* A table being constructed by CREATE TABLE */
  //Trigger *pNewTrigger;     /* Trigger under construct by a CREATE TRIGGER */
  const char *zAuthContext; /* The 6th parameter to db->xAuth callbacks */
  Token sNameToken;         /* Token with unqualified schema object name */
  Token sLastToken;         /* The last token parsed */
#ifndef SQLITE_OMIT_VIRTUALTABLE
  Token sArg;               /* Complete text of a module argument */
  //Table **apVtabLock;       /* Pointer to virtual tables needing locking */
#endif
  //Table *pZombieTab;        /* List of Table objects to delete after code gen */
  //TriggerPrg *pTriggerPrg;  /* Linked list of coded triggers */
};
/*
** The testcase() macro is used to aid in coverage testing.  When 
** doing coverage testing, the condition inside the argument to
** testcase() must be evaluated both true and false in order to
** get full branch coverage.  The testcase() macro is inserted
** to help ensure adequate test coverage in places where simple
** condition/decision coverage is inadequate.  For example, testcase()
** can be used to make sure boundary values are tested.  For
** bitmask tests, testcase() can be used to make sure each bit
** is significant and used at least once.  On switch statements
** where multiple cases go to the same block of code, testcase()
** can insure that all cases are evaluated.
**
*/
#ifdef SQLITE_COVERAGE_TEST
void sqlite3Coverage(int);
# define testcase(X)  if( X ){ sqlite3Coverage(__LINE__); }
#else
# define testcase(X)
#endif


/*
** The ALWAYS and NEVER macros surround boolean expressions which 
** are intended to always be true or false, respectively.  Such
** expressions could be omitted from the code completely.  But they
** are included in a few cases in order to enhance the resilience
** of SQLite to unexpected behavior - to make the code "self-healing"
** or "ductile" rather than being "brittle" and crashing at the first
** hint of unplanned behavior.
**
** In other words, ALWAYS and NEVER are added for defensive code.
**
** When doing coverage testing ALWAYS and NEVER are hard-coded to
** be true and false so that the unreachable code then specify will
** not be counted as untested code.
*/
#if defined(SQLITE_COVERAGE_TEST)
# define ALWAYS(X)      (1)
# define NEVER(X)       (0)
#elif !defined(NDEBUG)
# define ALWAYS(X)      ((X)?1:(assert(0),0))
# define NEVER(X)       ((X)?(assert(0),1):0)
#else
# define ALWAYS(X)      (X)
# define NEVER(X)       (X)
#endif


#define sqlite3ParserTOKENTYPE Token
#define sqlite3ParserARG_SDECL Parse *pParse;


/*
** These macros can be used to test, set, or clear bits in the 
** Expr.flags field.
*/
#define ExprHasProperty(E,P)     (((E)->flags&(P))==(P))
#define ExprHasAnyProperty(E,P)  (((E)->flags&(P))!=0)
#define ExprSetProperty(E,P)     (E)->flags|=(P)
#define ExprClearProperty(E,P)   (E)->flags&=~(P)


typedef struct Expr Expr;
typedef struct ExprList ExprList;
typedef struct ExprSpan ExprSpan;
typedef struct Select Select;
typedef struct IdList IdList;
typedef struct Column Column;
typedef struct Table Table;
typedef struct Schema Schema;
typedef struct SrcList SrcList;
typedef struct TriggerStep TriggerStep;
typedef struct Trigger Trigger;
/* Forward declarations of structures. */
typedef struct Hash Hash;
typedef struct HashElem HashElem;

struct Expr {
    u8 op;                 /* Operation performed by this node */
    char affinity;         /* The affinity of the column or 0 if not a column */
    u16 flags;             /* Various flags.  EP_* See below */
    union {
        char *zToken;          /* Token value. Zero terminated and dequoted */
        int iValue;            /* Non-negative integer value if EP_IntValue */
    } u;


  /* If the EP_TokenOnly flag is set in the Expr.flags mask, then no
  ** space is allocated for the fields below this point. An attempt to
  ** access them will result in a segfault or malfunction. 
  *********************************************************************/

  Expr *pLeft;           /* Left subnode */
  Expr *pRight;          /* Right subnode */
  union {
    ExprList *pList;     /* Function arguments or in "<expr> IN (<expr-list)" */
    Select *pSelect;     /* Used for sub-selects and "<expr> IN (<select>)" */
  } x;
  
  /* If the EP_Reduced flag is set in the Expr.flags mask, then no
  ** space is allocated for the fields below this point. An attempt to
  ** access them will result in a segfault or malfunction.
  *********************************************************************/

#if SQLITE_MAX_EXPR_DEPTH>0
  int nHeight;           /* Height of the tree headed by this node */
#endif
  int iTable;            /* TK_COLUMN: cursor number of table holding column
                         ** TK_REGISTER: register number
                         ** TK_TRIGGER: 1 -> new, 0 -> old */
  //ynVar iColumn;         /* TK_COLUMN: column index.  -1 for rowid.
                         /** TK_VARIABLE: variable number (always >= 1). */
  i16 iAgg;              /* Which entry in pAggInfo->aCol[] or ->aFunc[] */
  i16 iRightJoinTable;   /* If EP_FromJoin, the right table of the join */
  u8 flags2;             /* Second set of flags.  EP2_... */
  u8 op2;                /* TK_REGISTER: original value of Expr.op
                         ** TK_COLUMN: the value of p5 for OP_Column
                         ** TK_AGG_FUNCTION: nesting depth */
  //AggInfo *pAggInfo;     /* Used by TK_AGG_COLUMN and TK_AGG_FUNCTION */
  Table *pTab;           /* Table for TK_COLUMN expressions. */
};


struct ExprList {
    int nExpr;             /* Number of expressions on the list */
    int iECursor;          /* VDBE Cursor associated with this ExprList */
    struct ExprList_item { /* For each expression in the list */
        Expr *pExpr;            /* The list of expressions */
        char *zName;            /* Token associated with this expression */
        char *zSpan;            /* Original text of the expression */
        u8 sortOrder;           /* 1 for DESC or 0 for ASC */
        unsigned done :1;       /* A flag to indicate when processing is finished */
        unsigned bSpanIsTab :1; /* zSpan holds DB.TABLE.COLUMN */
        u16 iOrderByCol;        /* For ORDER BY, column number in result set */
        u16 iAlias;             /* Index into Parse.aAlias[] for zName */
    } *a;                  /* Alloc a power of two greater or equal to nExpr */
};



/*
** An instance of this structure is used by the parser to record both
** the parse tree for an expression and the span of input text for an
** expression.
*/
struct ExprSpan {
    Expr *pExpr;          /* The expression parse tree */
    const char *zStart;   /* First character of input text */
    const char *zEnd;     /* One character past the end of input text */
};
/*
** An instance of this structure can hold a simple list of identifiers,
** such as the list "a,b,c" in the following statements:
**
**      INSERT INTO t(a,b,c) VALUES ...;
**      CREATE INDEX idx ON t(a,b,c);
**      CREATE TRIGGER trig BEFORE UPDATE ON t(a,b,c) ...;
**
** The IdList.a.idx field is used when the IdList represents the list of
** column names after a table name in an INSERT statement.  In the statement
**
**     INSERT INTO t(a,b,c) ...
**
** If "a" is the k-th column of table "t", then IdList.a[0].idx==k.
*/
struct IdList {
    struct IdList_item {
        char *zName;      /* Name of the identifier */
        int idx;          /* Index in some Table.aCol[] of a column named zName */
    } *a;
    int nId;         /* Number of identifiers on the list */
};

/*
** Constant tokens for values 0 and 1.
*/
const Token sqlite3IntTokens[] = {
    { "0", 1 },
    { "1", 1 }
};




/* Each element in the hash table is an instance of the following 
** structure.  All elements are stored on a single doubly-linked list.
**
** Again, this structure is intended to be opaque, but it can't really
** be opaque because it is used by macros.
*/
struct HashElem {
    HashElem *next, *prev;       /* Next and previous elements in the table */
    void *data;                  /* Data associated with this element */
    const char *pKey; int nKey;  /* Key associated with this element */
};


/*
 * Each trigger present in the database schema is stored as an instance of
 * struct Trigger. 
 *
 * Pointers to instances of struct Trigger are stored in two ways.
 * 1. In the "trigHash" hash table (part of the sqlite3* that represents the 
 *    database). This allows Trigger structures to be retrieved by name.
 * 2. All triggers associated with a single table form a linked list, using the
 *    pNext member of struct Trigger. A pointer to the first element of the
 *    linked list is stored as the "pTrigger" member of the associated
 *    struct Table.
 *
 * The "step_list" member points to the first element of a linked list
 * containing the SQL statements specified as the trigger program.
 */
struct Trigger {
  char *zName;            /* The name of the trigger                        */
  char *table;            /* The table or view to which the trigger applies */
  u8 op;                  /* One of TK_DELETE, TK_UPDATE, TK_INSERT         */
  u8 tr_tm;               /* One of TRIGGER_BEFORE, TRIGGER_AFTER */
  Expr *pWhen;            /* The WHEN clause of the expression (may be NULL) */
  IdList *pColumns;       /* If this is an UPDATE OF <column-list> trigger,
                             the <column-list> is stored here */
  Schema *pSchema;        /* Schema containing the trigger */
  Schema *pTabSchema;     /* Schema containing the table */
  TriggerStep *step_list; /* Link list of trigger program steps             */
  Trigger *pNext;         /* Next trigger associated with the table */
};


struct TriggerStep {
    u8 op;               /* One of TK_DELETE, TK_UPDATE, TK_INSERT, TK_SELECT */
    u8 orconf;           /* OE_Rollback etc. */
    Trigger *pTrig;      /* The trigger that this step is a part of */
    Select *pSelect;     /* SELECT statment or RHS of INSERT INTO .. SELECT ... */
    Token target;        /* Target table for DELETE, UPDATE, INSERT */
    Expr *pWhere;        /* The WHERE clause for DELETE or UPDATE steps */
    ExprList *pExprList; /* SET clause for UPDATE.  VALUES clause for INSERT */
    IdList *pIdList;     /* Column names for INSERT */
    TriggerStep *pNext;  /* Next in the link-list */
    TriggerStep *pLast;  /* Last element in link-list. Valid for 1st elem only */
};

#define UNUSED_PARAMETER(x) (void)(x)
#define UNUSED_PARAMETER2(x,y) UNUSED_PARAMETER(x),UNUSED_PARAMETER(y)


#define SQLITE_LIMIT_LENGTH                    0
#define SQLITE_LIMIT_SQL_LENGTH                1
#define SQLITE_LIMIT_COLUMN                    2
#define SQLITE_LIMIT_EXPR_DEPTH                3
#define SQLITE_LIMIT_COMPOUND_SELECT           4
#define SQLITE_LIMIT_VDBE_OP                   5
#define SQLITE_LIMIT_FUNCTION_ARG              6
#define SQLITE_LIMIT_ATTACHED                  7
#define SQLITE_LIMIT_LIKE_PATTERN_LENGTH       8
#define SQLITE_LIMIT_VARIABLE_NUMBER           9
#define SQLITE_LIMIT_TRIGGER_DEPTH            10

/*
** SQLite supports many different ways to resolve a constraint
** error.  ROLLBACK processing means that a constraint violation
** causes the operation in process to fail and for the current transaction
** to be rolled back.  ABORT processing means the operation in process
** fails and any prior changes from that one operation are backed out,
** but the transaction is not rolled back.  FAIL processing means that
** the operation in progress stops and returns an error code.  But prior
** changes due to the same operation are not backed out and no rollback
** occurs.  IGNORE means that the particular row that caused the constraint
** error is not inserted or updated.  Processing continues and no error
** is returned.  REPLACE means that preexisting database rows that caused
** a UNIQUE constraint violation are removed so that the new insert or
** update can proceed.  Processing continues and no error is reported.
**
** RESTRICT, SETNULL, and CASCADE actions apply only to foreign keys.
** RESTRICT is the same as ABORT for IMMEDIATE foreign keys and the
** same as ROLLBACK for DEFERRED keys.  SETNULL means that the foreign
** key is set to NULL.  CASCADE means that a DELETE or UPDATE of the
** referenced table row is propagated into the row that holds the
** foreign key.
** 
** The following symbolic values are used to record which type
** of action to take.
*/
#define OE_None     0   /* There is no constraint to check */
#define OE_Rollback 1   /* Fail the operation and rollback the transaction */
#define OE_Abort    2   /* Back out changes but do no rollback transaction */
#define OE_Fail     3   /* Stop the operation but leave all prior changes */
#define OE_Ignore   4   /* Ignore the error. Do not do the INSERT or UPDATE */
#define OE_Replace  5   /* Delete existing record, then do INSERT or UPDATE */

#define OE_Restrict 6   /* OE_Abort for IMMEDIATE, OE_Rollback for DEFERRED */
#define OE_SetNull  7   /* Set the foreign key value to NULL */
#define OE_SetDflt  8   /* Set the foreign key value to its default */
#define OE_Cascade  9   /* Cascade the changes */

#define OE_Default  99  /* Do whatever the default action is */

/*
** The following are the meanings of bits in the Expr.flags field.
*/
#define EP_FromJoin   0x0001  /* Originated in ON or USING clause of a join */
#define EP_Agg        0x0002  /* Contains one or more aggregate functions */
#define EP_Resolved   0x0004  /* IDs have been resolved to COLUMNs */
#define EP_Error      0x0008  /* Expression contains one or more errors */
#define EP_Distinct   0x0010  /* Aggregate function with DISTINCT keyword */
#define EP_VarSelect  0x0020  /* pSelect is correlated, not constant */
#define EP_DblQuoted  0x0040  /* token.z was originally in "..." */
#define EP_InfixFunc  0x0080  /* True for an infix function: LIKE, GLOB, etc */
#define EP_Collate    0x0100  /* Tree contains a TK_COLLATE opeartor */
#define EP_FixedDest  0x0200  /* Result needed in a specific register */
#define EP_IntValue   0x0400  /* Integer value contained in u.iValue */
#define EP_xIsSelect  0x0800  /* x.pSelect is valid (otherwise x.pList is) */
#define EP_Hint       0x1000  /* Not used */
#define EP_Reduced    0x2000  /* Expr struct is EXPR_REDUCEDSIZE bytes only */
#define EP_TokenOnly  0x4000  /* Expr struct is EXPR_TOKENONLYSIZE bytes only */
#define EP_Static     0x8000  /* Held in memory not obtained from malloc() */


/* A complete hash table is an instance of the following structure.
** The internals of this structure are intended to be opaque -- client
** code should not attempt to access or modify the fields of this structure
** directly.  Change this structure only by using the routines below.
** However, some of the "procedures" and "functions" for modifying and
** accessing this structure are really macros, so we can't really make
** this structure opaque.
**
** All elements of the hash table are on a single doubly-linked list.
** Hash.first points to the head of this list.
**
** There are Hash.htsize buckets.  Each bucket points to a spot in
** the global doubly-linked list.  The contents of the bucket are the
** element pointed to plus the next _ht.count-1 elements in the list.
**
** Hash.htsize and Hash.ht may be zero.  In that case lookup is done
** by a linear search of the global list.  For small tables, the 
** Hash.ht table is never allocated because if there are few elements
** in the table, it is faster to do a linear search than to manage
** the hash table.
*/
struct Hash {
    unsigned int htsize;      /* Number of buckets in the hash table */
    unsigned int count;       /* Number of entries in this table */
    HashElem *first;          /* The first element of the array */
    struct _ht {              /* the hash table */
        int count;                 /* Number of entries with this hash */
        HashElem *chain;           /* Pointer to first entry with this hash */
    } *ht;
};


/*
** information about each column of an SQL table is held in an instance
** of this structure.
*/
struct Column {
    char *zName;     /* Name of this column */
    Expr *pDflt;     /* Default value of this column */
    char *zDflt;     /* Original text of the default value */
    char *zType;     /* Data type for this column */
    char *zColl;     /* Collating sequence.  If NULL, use the default */
    u8 notNull;      /* An OE_ code for handling a NOT NULL constraint */
    char affinity;   /* One of the SQLITE_AFF_... values */
    u16 colFlags;    /* Boolean properties.  See COLFLAG_ defines below */
};


/*
** Each SQL table is represented in memory by an instance of the
** following structure.
**
** Table.zName is the name of the table.  The case of the original
** CREATE TABLE statement is stored, but case is not significant for
** comparisons.
**
** Table.nCol is the number of columns in this table.  Table.aCol is a
** pointer to an array of Column structures, one for each column.
**
** If the table has an INTEGER PRIMARY KEY, then Table.iPKey is the index of
** the column that is that key.   Otherwise Table.iPKey is negative.  Note
** that the datatype of the PRIMARY KEY must be INTEGER for this field to
** be set.  An INTEGER PRIMARY KEY is used as the rowid for each row of
** the table.  If a table has no INTEGER PRIMARY KEY, then a random rowid
** is generated for each row of the table.  TF_HasPrimaryKey is set if
** the table has any PRIMARY KEY, INTEGER or otherwise.
**
** Table.tnum is the page number for the root BTree page of the table in the
** database file.  If Table.iDb is the index of the database table backend
** in sqlite.aDb[].  0 is for the main database and 1 is for the file that
** holds temporary tables and indices.  If TF_Ephemeral is set
** then the table is stored in a file that is automatically deleted
** when the VDBE cursor to the table is closed.  In this case Table.tnum 
** refers VDBE cursor number that holds the table open, not to the root
** page number.  Transient tables are used to hold the results of a
** sub-query that appears instead of a real table name in the FROM clause 
** of a SELECT statement.
*/
struct Table {
    char *zName;         /* Name of the table or view */
    Column *aCol;        /* Information about each column */
    //Index *pIndex;       /* List of SQL indexes on this table. */
    Select *pSelect;     /* NULL for tables.  Points to definition if a view. */
    //FKey *pFKey;         /* Linked list of all foreign keys in this table */
    char *zColAff;       /* String defining the affinity of each column */
#ifndef SQLITE_OMIT_CHECK
    ExprList *pCheck;    /* All CHECK constraints */
#endif
    //tRowcnt nRowEst;     /* Estimated rows in table - from sqlite_stat1 table */
    int tnum;            /* Root BTree node for this table (see note above) */
    i16 iPKey;           /* If not negative, use aCol[iPKey] as the primary key */
    i16 nCol;            /* Number of columns in this table */
    u16 nRef;            /* Number of pointers to this Table */
    u8 tabFlags;         /* Mask of TF_* values */
    u8 keyConf;          /* What to do in case of uniqueness conflict on iPKey */
#ifndef SQLITE_OMIT_ALTERTABLE
    int addColOffset;    /* Offset in CREATE TABLE stmt to add a new column */
#endif
#ifndef SQLITE_OMIT_VIRTUALTABLE
    int nModuleArg;      /* Number of arguments to the module */
    char **azModuleArg;  /* Text of all module args. [0] is module name */
    //VTable *pVTable;     /* List of VTable objects. */
#endif
    //Trigger *pTrigger;   /* List of triggers stored in pSchema */
    Schema *pSchema;     /* Schema that contains this table */
    Table *pNextZombie;  /* Next on the Parse.pZombieTab list */
};

/*
** An instance of the following structure stores a database schema.
**
** Most Schema objects are associated with a Btree.  The exception is
** the Schema for the TEMP databaes (sqlite3.aDb[1]) which is free-standing.
** In shared cache mode, a single Schema object can be shared by multiple
** Btrees that refer to the same underlying BtShared object.
** 
** Schema objects are automatically deallocated when the last Btree that
** references them is destroyed.   The TEMP Schema is manually freed by
** sqlite3_close().
*
** A thread must be holding a mutex on the corresponding Btree in order
** to access Schema content.  This implies that the thread must also be
** holding a mutex on the sqlite3 connection pointer that owns the Btree.
** For a TEMP Schema, only the connection mutex is required.
*/
struct Schema {
    int schema_cookie;   /* Database schema version number for this file */
    int iGeneration;     /* Generation counter.  Incremented with each change */
    Hash tblHash;        /* All tables indexed by name */
    Hash idxHash;        /* All (named) indices indexed by name */
    Hash trigHash;       /* All triggers indexed by name */
    Hash fkeyHash;       /* All foreign keys by referenced table name */
    Table *pSeqTab;      /* The sqlite_sequence table used by AUTOINCREMENT */
    u8 file_format;      /* Schema format version for this file */
    u8 enc;              /* Text encoding used by this database */
    u16 flags;           /* Flags associated with this schema */
    int cache_size;      /* Number of pages to use in the cache */
};

/*
** The following structure describes the FROM clause of a SELECT statement.
** Each table or subquery in the FROM clause is a separate element of
** the SrcList.a[] array.
**
** With the addition of multiple database support, the following structure
** can also be used to describe a particular table such as the table that
** is modified by an INSERT, DELETE, or UPDATE statement.  In standard SQL,
** such a table must be a simple name: ID.  But in SQLite, the table can
** now be identified by a database name, a dot, then the table name: ID.ID.
**
** The jointype starts out showing the join type between the current table
** and the next table on the list.  The parser builds the list this way.
** But sqlite3SrcListShiftJoinType() later shifts the jointypes so that each
** jointype expresses the join between the table and the previous table.
**
** In the colUsed field, the high-order bit (bit 63) is set if the table
** contains more than 63 columns and the 64-th or later column is used.
*/
struct SrcList {
    i16 nSrc;        /* Number of tables or subqueries in the FROM clause */
    i16 nAlloc;      /* Number of entries allocated in a[] below */
    struct SrcList_item {
        Schema *pSchema;  /* Schema to which this item is fixed */
        char *zDatabase;  /* Name of database holding this table */
        char *zName;      /* Name of the table */
        char *zAlias;     /* The "B" part of a "A AS B" phrase.  zName is the "A" */
        Table *pTab;      /* An SQL table corresponding to zName */
        Select *pSelect;  /* A SELECT statement used in place of a table name */
        int addrFillSub;  /* Address of subroutine to manifest a subquery */
        int regReturn;    /* Register holding return address of addrFillSub */
        u8 jointype;      /* Type of join between this able and the previous */
        unsigned notIndexed :1;    /* True if there is a NOT INDEXED clause */
        unsigned isCorrelated :1;  /* True if sub-query is correlated */
        unsigned viaCoroutine :1;  /* Implemented as a co-routine */
#ifndef SQLITE_OMIT_EXPLAIN
        u8 iSelectId;     /* If pSelect!=0, the id of the sub-select in EQP */
#endif
        int iCursor;      /* The VDBE cursor number used to access this table */
        Expr *pOn;        /* The ON clause of a join */
        IdList *pUsing;   /* The USING clause of a join */
        //Bitmask colUsed;  /* Bit N (1<<N) set if column N of pTab is used */
        char *zIndex;     /* Identifier from "INDEXED BY <zIndex>" clause */
        //Index *pIndex;    /* Index structure corresponding to zIndex, if any */
    } a[1];             /* One entry for each identifier on the list */
};


/*
** An instance of the following structure contains all information
** needed to generate code for a single SELECT statement.
**
** nLimit is set to -1 if there is no LIMIT clause.  nOffset is set to 0.
** If there is a LIMIT clause, the parser sets nLimit to the value of the
** limit and nOffset to the value of the offset (or 0 if there is not
** offset).  But later on, nLimit and nOffset become the memory locations
** in the VDBE that record the limit and offset counters.
**
** addrOpenEphm[] entries contain the address of OP_OpenEphemeral opcodes.
** These addresses must be stored so that we can go back and fill in
** the P4_KEYINFO and P2 parameters later.  Neither the KeyInfo nor
** the number of columns in P2 can be computed at the same time
** as the OP_OpenEphm instruction is coded because not
** enough information about the compound query is known at that point.
** The KeyInfo for addrOpenTran[0] and [1] contains collating sequences
** for the result set.  The KeyInfo for addrOpenEphm[2] contains collating
** sequences for the ORDER BY clause.
*/
struct Select {
    ExprList *pEList;      /* The fields of the result */
    u8 op;                 /* One of: TK_UNION TK_ALL TK_INTERSECT TK_EXCEPT */
    u16 selFlags;          /* Various SF_* values */
    int iLimit, iOffset;   /* Memory registers holding LIMIT & OFFSET counters */
    int addrOpenEphm[3];   /* OP_OpenEphem opcodes related to this select */
    double nSelectRow;     /* Estimated number of result rows */
    SrcList *pSrc;         /* The FROM clause */
    Expr *pWhere;          /* The WHERE clause */
    ExprList *pGroupBy;    /* The GROUP BY clause */
    Expr *pHaving;         /* The HAVING clause */
    ExprList *pOrderBy;    /* The ORDER BY clause */
    Select *pPrior;        /* Prior select in a compound select statement */
    Select *pNext;         /* Next select to the left in a compound */
    Select *pRightmost;    /* Right-most select in a compound select statement */
    Expr *pLimit;          /* LIMIT expression. NULL means not used. */
    Expr *pOffset;         /* OFFSET expression. NULL means not used. */
};

/*
** An instance of this object describes where to put of the results of
** a SELECT statement.
*/
struct SelectDest {
    u8 eDest;         /* How to dispose of the results.  On of SRT_* above. */
    char affSdst;     /* Affinity used when eDest==SRT_Set */
    int iSDParm;      /* A parameter used by the eDest disposal method */
    int iSdst;        /* Base register where results are written */
    int nSdst;        /* Number of registers allocated */
};

Expr *sqlite3PExpr(
    Parse *pParse,          /* Parsing context */
    int op,                 /* Expression opcode */
    Expr *pLeft,            /* Left operand */
    Expr *pRight,           /* Right operand */
    const Token *pToken     /* Argument token */
    );

Select * sqlite3SelectNew( Parse *pParse, /* Parsing context */ ExprList *pEList, /* which columns to include in the result */ SrcList *pSrc, /* the FROM clause -- which tables to scan */ Expr *pWhere, /* the WHERE clause */ ExprList *pGroupBy, /* the GROUP BY clause */ Expr *pHaving, /* the HAVING clause */ ExprList *pOrderBy, /* the ORDER BY clause */ u16 selFlags, /* true if the DISTINCT keyword is present */ Expr *pLimit, /* LIMIT value. NULL means not used */ Expr *pOffset /* OFFSET value. NULL means no offset */ );
int sqlite3Select(
    Parse *pParse,         /* The parser context */
    Select *p,             /* The SELECT statement being coded. */
    SelectDest *pDest      /* What to do with the query results */
    );


SrcList *sqlite3SrcListAppend(
    SrcList *pList,     /* Append to this SrcList. NULL creates a new SrcList */
    Token *pTable,      /* Table to append */
    Token *pDatabase    /* Database of the table */
    );


/** This routine is called by the parser to add a new term to the
    ** end of a growing FROM clause.  The "p" parameter is the part of
    ** the FROM clause that has already been constructed.  "p" is NULL
    ** if this is the first term of the FROM clause.  pTable and pDatabase
    ** are the name of the table and database named in the FROM clause term.
    ** pDatabase is NULL if the database name qualifier is missing - the
    ** usual case.  If the term has a alias, then pAlias points to the
    ** alias token.  If the term is a subquery, then pSubquery is the
    ** SELECT statement that the subquery encodes.  The pTable and
    ** pDatabase parameters are NULL for subqueries.  The pOn and pUsing
    ** parameters are the content of the ON and USING clauses.
    **
    ** Return a new SrcList which encodes is the FROM with the new
    ** term added.
    */
SrcList *sqlite3SrcListAppendFromTerm(
    Parse *pParse,          /* Parsing context */
    SrcList *p,             /* The left part of the FROM clause already seen */
    Token *pTable,          /* Name of the table to add to the FROM clause */
    Token *pDatabase,       /* Name of the database containing pTable */
    Token *pAlias,          /* The right-hand side of the AS subexpression */
    Select *pSubquery,      /* A subquery used in place of a table name */
    Expr *pOn,              /* The ON clause of a join */
    IdList *pUsing          /* The USING clause of a join */
    );

ExprList *sqlite3ExprListAppend(
    Parse *pParse,          /* Parsing context */
    ExprList *pList,        /* List to which to append. Might be NULL */
    Expr *pExpr             /* Expression to be appended. Might be NULL */
    );

/*
** Set the ExprList.a[].zName element of the most recently added item
** on the expression list.
**
** pList might be NULL following an OOM error.  But pName should never be
** NULL.  If a memory allocation fails, the pParse->db->mallocFailed flag
** is set.
*/
void sqlite3ExprListSetName(
    Parse *pParse,          /* Parsing context */
    ExprList *pList,        /* List to which to add the span. */
    Token *pName,           /* Name to be added */
    int dequote             /* True to cause the name to be dequoted */
    );


void sqlite3ExprListSetSpan(
    Parse *pParse,          /* Parsing context */
    ExprList *pList,        /* List to which to add the span. */
    ExprSpan *pSpan         /* The span to be added */
    );

void sqlite3ErrorMsg(char* param2 );
Expr *sqlite3ExprAlloc(int,const Token*,int);


TriggerStep *sqlite3DeleteTriggerStep( Parse *pParse, int param23 );


void sqlite3StartTable( Parse *pParse, void * yymsp, void * yymsp4, void * yymsp3, int param5, int param6, void * yymsp2 );


void sqlite3Savepoint( Parse *pParse, int  isSavePoint, void * yymsp );


void sqlite3EndTable( Parse *pParse, void * yymsp, void * yymsp2, int param4 );


void sqlite3SelectDelete(Select *p);



void sqlite3IdListDelete(IdList *pList);



void sqlite3ExprDelete(Expr *p);
void sqlite3DbFree(void*);

void sqlite3ExprListDelete(ExprList *pList);



TriggerStep * sqlite3TriggerDeleteStep( void *db, void * yymsp, void * yymsp1 );


TriggerStep * sqlite3TriggerInsertStep( void *db, void * yymsp, void * yymsp1, ExprList* param4, void * yymsp2, u8 yymsp3 );


TriggerStep * sqlite3TriggerUpdateStep( void *db, void * yymsp, void * yymsp1, void * yymsp2, u8 yymsp3 );


TriggerStep * sqlite3TriggerSelectStep( void *db, void * yymsp );


void* sqlite3DbMallocZero(int n );
int sqlite3Dequote(char *z);
Expr * sqlite3ExprFunction( Parse *pParse, ExprList * pList, Token *pToken);
void sqlite3ExprAttachSubtrees(
    Expr *pRoot,
    Expr *pLeft,
    Expr *pRight
    );

int sqlite3JoinType(Parse *pParse, Token *pA, Token *pB, Token *pC);

IdList* sqlite3IdListAppend( void* db, void * yymsp, void * yymsp2 );

Expr *sqlite3ExprAddCollateToken(Parse *pParse, Expr *pExpr, Token *pCollName);

int sqlite3ExprIsInteger(Expr *p, int *pValue);
char *sqlite3DbStrNDup(const char *z, int n);
int sqlite3GetInt32(const char *zNum, int *pValue);
void sqlite3SrcListDelete(SrcList *pList);
void sqlite3SrcListIndexedBy(Parse *pParse, SrcList *p, Token *pIndexedBy);
char *sqlite3NameFromToken(Token *pName);
void sqlite3SrcListShiftJoinType(SrcList *p);
Expr *sqlite3Expr(
    int op,                 /* Expression opcode */
    const char *zToken      /* Token argument.  Might be NULL */
    );
int sqlite3Strlen30(const char*);
int sqlite3_strnicmp(const char *zLeft, const char *zRight, int N);
#endif // SERVER_MANAGER_sqliteInt_h__
