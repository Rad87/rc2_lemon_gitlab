#include "sqliteInt.h"
#include "parse2.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


void sqlite3Savepoint( Parse *pParse, int isSavePoint, void * yymsp )
{
    //assert(!"The method or operation is not implemented.");
}

void sqlite3EndTable( Parse *pParse, void * yymsp, void * yymsp2, int param4 )
{
    //assert(!"The method or operation is not implemented.");
}


/*
** Delete all the content of a Select structure but do not deallocate
** the select structure itself.
*/
static void clearSelect(Select *p){
    sqlite3ExprListDelete(p->pEList);
    sqlite3SrcListDelete(p->pSrc);
    sqlite3ExprDelete(p->pWhere);
    sqlite3ExprListDelete(p->pGroupBy);
    sqlite3ExprDelete(p->pHaving);
    sqlite3ExprListDelete(p->pOrderBy);
    sqlite3SelectDelete(p->pPrior);
    sqlite3ExprDelete(p->pLimit);
    sqlite3ExprDelete(p->pOffset);
}


/*
** Delete the given Select structure and all of its substructures.
*/
void sqlite3SelectDelete(Select *p)
{
    if( p ){
        clearSelect(p);
        sqlite3DbFree(p);
    }
}


/*
** Delete an IdList.
*/
void sqlite3IdListDelete(IdList *pList){
    int i;
    if( pList==0 ) return;
    for(i=0; i<pList->nId; i++){
        sqlite3DbFree(pList->a[i].zName);
    }
    sqlite3DbFree(pList->a);
    sqlite3DbFree(pList);
}



/*
** Set the collating sequence for expression pExpr to be the collating
** sequence named by pToken.   Return a pointer to a new Expr node that
** implements the COLLATE operator.
**
** If a memory allocation error occurs, that fact is recorded in pParse->db
** and the pExpr parameter is returned unchanged.
*/
Expr *sqlite3ExprAddCollateToken(Parse *pParse, Expr *pExpr, Token *pCollName){
    if( pCollName->n>0 ){
        Expr *pNew = sqlite3ExprAlloc(TK_COLLATE, pCollName, 1);
        if( pNew ){
            pNew->pLeft = pExpr;
            pNew->flags |= EP_Collate;
            pExpr = pNew;
        }
    }
    return pExpr;
}

IdList* sqlite3IdListAppend( void* db, void * yymsp, void * yymsp2 )
{
    // assert(!"The method or operation is not implemented.");
    return 0;
}
int sqlite3_strnicmp(const char *zLeft, const char *zRight, int N){
    register unsigned char *a, *b;
    a = (unsigned char *)zLeft;
    b = (unsigned char *)zRight;
    while( N-- > 0 && *a!=0 && UpperToLower[*a]==UpperToLower[*b]){ a++; b++; }
    return N<0 ? 0 : UpperToLower[*a] - UpperToLower[*b];
}

int sqlite3JoinType(Parse *pParse, Token *pA, Token *pB, Token *pC){
    int jointype = 0;
    Token *apAll[3];
    Token *p;
    /*   0123456789 123456789 123456789 123 */
    static const char zKeyText[] = "naturaleftouterightfullinnercross";
    static const struct {
        u8 i;        /* Beginning of keyword text in zKeyText[] */
        u8 nChar;    /* Length of the keyword in characters */
        u8 code;     /* Join type mask */
    } aKeyword[] = {
        /* natural */ { 0,  7, JT_NATURAL                },
        /* left    */ { 6,  4, JT_LEFT|JT_OUTER          },
        /* outer   */ { 10, 5, JT_OUTER                  },
        /* right   */ { 14, 5, JT_RIGHT|JT_OUTER         },
        /* full    */ { 19, 4, JT_LEFT|JT_RIGHT|JT_OUTER },
        /* inner   */ { 23, 5, JT_INNER                  },
        /* cross   */ { 28, 5, JT_INNER|JT_CROSS         },
    };
    int i, j;
    apAll[0] = pA;
    apAll[1] = pB;
    apAll[2] = pC;
    for(i=0; i<3 && apAll[i]; i++){
        p = apAll[i];
        for(j=0; j<ArraySize(aKeyword); j++){
            if( p->n==aKeyword[j].nChar 
                && sqlite3StrNICmp((char*)p->z, &zKeyText[aKeyword[j].i], p->n)==0 ){
                    jointype |= aKeyword[j].code;
                    break;
            }
        }
        testcase( j==0 || j==1 || j==2 || j==3 || j==4 || j==5 || j==6 );
        if( j>=ArraySize(aKeyword) ){
            jointype |= JT_ERROR;
            break;
        }
    }
    if(
        (jointype & (JT_INNER|JT_OUTER))==(JT_INNER|JT_OUTER) ||
        (jointype & JT_ERROR)!=0
        ){
            const char *zSp = " ";
            assert( pB!=0 );
            if( pC==0 ){ zSp++; }
            sqlite3ErrorMsg("unknown or unsupported join type: ");
            jointype = JT_INNER;
    }else if( (jointype & JT_OUTER)!=0 
        && (jointype & (JT_LEFT|JT_RIGHT))!=JT_LEFT ){
            sqlite3ErrorMsg("RIGHT and FULL OUTER JOINs are not currently supported");
            jointype = JT_INNER;
    }
    return jointype;
}

Expr * sqlite3ExprFunction( Parse *pParse, ExprList * pList, Token *pToken)
{
    Expr *pNew;
    assert( pToken );
    pNew = sqlite3ExprAlloc(TK_FUNCTION, pToken, 1);
    if( pNew==0 ){
        sqlite3ExprListDelete(pList); /* Avoid memory leak when malloc fails */
        return 0;
    }
    pNew->x.pList = pList;
    assert( !ExprHasProperty(pNew, EP_xIsSelect) );
    //sqlite3ExprSetHeight(pParse, pNew);
    return pNew;
}

/*
** Allocate and zero memory.  If the allocation fails, make
** the mallocFailed flag in the connection pointer.
*/
void *sqlite3DbMallocZero(int n){
    return calloc(n, 1);
}

TriggerStep * sqlite3TriggerSelectStep( void *db, void * yymsp )
{
    return 0;
    //assert(!"The method or operation is not implemented.");
}

TriggerStep * sqlite3TriggerUpdateStep( void *db, void * yymsp, void * yymsp1, void * yymsp2, u8 yymsp3 )
{
    return 0;
    //assert(!"The method or operation is not implemented.");
}

TriggerStep * sqlite3TriggerInsertStep( void *db, void * yymsp, void * yymsp1, ExprList* param4, void * yymsp2, u8 yymsp3 )
{
    return 0;
    //assert(!"The method or operation is not implemented.");
}

TriggerStep * sqlite3TriggerDeleteStep( void *db, void * yymsp, void * yymsp1 )
{
    //assert(!"The method or operation is not implemented.");
    return 0;
}

/*
** Delete an entire expression list.
*/
void sqlite3ExprListDelete(ExprList *pList){
    int i;
    ExprList::ExprList_item *pItem;
    if( pList==0 ) return;
    for(pItem=pList->a, i=0; i<pList->nExpr; i++, pItem++){
        sqlite3ExprDelete(pItem->pExpr);
        sqlite3DbFree(pItem->zName);
        sqlite3DbFree(pItem->zSpan);
    }
    sqlite3DbFree(pList->a);
    sqlite3DbFree(pList);
}
/*
** Free memory that might be associated with a particular database
** connection.
*/
void sqlite3DbFree(void *p){
    free(p);
}

/*
** Add an INDEXED BY or NOT INDEXED clause to the most recently added 
** element of the source-list passed as the second argument.
*/
void sqlite3SrcListIndexedBy(Parse *pParse, SrcList *p, Token *pIndexedBy){
  assert( pIndexedBy!=0 );
  if( p && ALWAYS(p->nSrc>0) ){
    SrcList::SrcList_item *pItem = &p->a[p->nSrc-1];
    assert( pItem->notIndexed==0 && pItem->zIndex==0 );
    if( pIndexedBy->n==1 && !pIndexedBy->z ){
      /* A "NOT INDEXED" clause was supplied. See parse.y 
      ** construct "indexed_opt" for details. */
      pItem->notIndexed = 1;
    }else{
      pItem->zIndex = sqlite3NameFromToken(pIndexedBy);
    }
  }
}

/*
** Delete an entire SrcList including all its substructure.
*/
void sqlite3SrcListDelete(SrcList *pList){
    int i;
    SrcList::SrcList_item *pItem;
    if( pList==0 ) return;
    for(pItem=pList->a, i=0; i<pList->nSrc; i++, pItem++){
        sqlite3DbFree(pItem->zDatabase);
        sqlite3DbFree(pItem->zName);
        sqlite3DbFree(pItem->zAlias);
        sqlite3DbFree(pItem->zIndex);
        /*sqlite3DeleteTable*/(pItem->pTab);
        sqlite3SelectDelete(pItem->pSelect);
        sqlite3ExprDelete(pItem->pOn);
        //sqlite3IdListDelete(pItem->pUsing);
    }
    sqlite3DbFree(pList);
}

/*
** Recursively delete an expression tree.
*/
void sqlite3ExprDelete(Expr *p){
    if( p==0 ) return;
    if( !ExprHasAnyProperty(p, EP_TokenOnly) ){
        sqlite3ExprDelete(p->pLeft);
        sqlite3ExprDelete(p->pRight);
        if( !ExprHasProperty(p, EP_Reduced) && (p->flags2 & EP2_MallocedToken)!=0 ){
            sqlite3DbFree(p->u.zToken);
        }
        if( ExprHasProperty(p, EP_xIsSelect) ){
            sqlite3SelectDelete(p->x.pSelect);
        }else{
            sqlite3ExprListDelete(p->x.pList);
        }
    }
    if( !ExprHasProperty(p, EP_Static) ){
        sqlite3DbFree(p);
    }
}

void sqlite3StartTable( Parse *pParse, void * yymsp, void * yymsp4, void * yymsp3, int param5, int param6, void * yymsp2 )
{
    return;
    //assert(!"The method or operation is not implemented.");
}

TriggerStep * sqlite3DeleteTriggerStep( Parse *pParse, int param23 )
{
    //assert(!"The method or operation is not implemented.");
    return 0;
}

void sqlite3ErrorMsg( char* param2 )
{
    return;
    //assert(!"The method or operation is not implemented.");
}

void sqlite3ExprListSetSpan( Parse *pParse, /* Parsing context */ ExprList *pList, /* List to which to add the span. */ ExprSpan *pSpan /* The span to be added */ )
{
    if( pList ){
        ExprList::ExprList_item *pItem = &pList->a[pList->nExpr-1];
        assert( pList->nExpr>0 );
        sqlite3DbFree(pItem->zSpan);
        pItem->zSpan = sqlite3DbStrNDup((char*)pSpan->zStart,
            (int)(pSpan->zEnd - pSpan->zStart));
    }
}

void sqlite3ExprListSetName( Parse *pParse, /* Parsing context */ ExprList *pList, /* List to which to add the span. */ Token *pName, /* Name to be added */ int dequote /* True to cause the name to be dequoted */ )
{
    return;
}

ExprList * sqlite3ExprListAppend( Parse *pParse, /* Parsing context */ ExprList *pList, /* List to which to append. Might be NULL */ Expr *pExpr /* Expression to be appended. Might be NULL */ )
{
    if( pList==0 ){
        pList = (ExprList *)sqlite3DbMallocZero(sizeof(ExprList) );
        if( pList==0 ){
            goto no_mem;
        }
        pList->a = (ExprList::ExprList_item*)malloc(sizeof(pList->a[0]));
        if( pList->a==0 ) goto no_mem;
    }else if( (pList->nExpr & (pList->nExpr-1))==0 ){
        ExprList::ExprList_item *a;
        assert( pList->nExpr>0 );
        a = (ExprList::ExprList_item*)realloc(pList->a, pList->nExpr*2*sizeof(pList->a[0]));
        if( a==0 ){
            goto no_mem;
        }
        pList->a = a;
    }
    assert( pList->a!=0 );
    if( 1 ){
        ExprList::ExprList_item *pItem = &pList->a[pList->nExpr++];
        memset(pItem, 0, sizeof(*pItem));
        pItem->pExpr = pExpr;
    }
    return pList;

no_mem:     
    /* Avoid leaking memory if malloc has failed. */
    sqlite3ExprDelete(pExpr);
    sqlite3ExprListDelete(pList);
    return 0;
}

SrcList * sqlite3SrcListAppendFromTerm( Parse *pParse, /* Parsing context */ SrcList *p, /* The left part of the FROM clause already seen */ Token *pTable, /* Name of the table to add to the FROM clause */ Token *pDatabase, /* Name of the database containing pTable */ Token *pAlias, /* The right-hand side of the AS subexpression */ Select *pSubquery, /* A subquery used in place of a table name */ Expr *pOn, /* The ON clause of a join */ IdList *pUsing /* The USING clause of a join */ )
{
    if( pTable) {
        char table[100];
        strncpy (table, pTable->z, pTable->n);
        table[pTable->n]=0;
        printf("Table  %s\n", table);
    }
    

    SrcList::SrcList_item *pItem;
    if( !p && (pOn || pUsing) ){
        /*sqlite3ErrorMsg("a JOIN clause is required before %s", 
        (pOn ? "ON" : "USING")
        );*/
        goto append_from_error;
    }
    p = sqlite3SrcListAppend(p, pTable, pDatabase);
    if( p==0 || NEVER(p->nSrc==0) ){
        goto append_from_error;
    }
    pItem = &p->a[p->nSrc-1];
    assert( pAlias!=0 );
    if( pAlias->n ){
        pItem->zAlias = sqlite3NameFromToken(pAlias);
    }
    pItem->pSelect = pSubquery;
    pItem->pOn = pOn;
    pItem->pUsing = pUsing;
    return p;

append_from_error:
    assert( p==0 );
    sqlite3ExprDelete(pOn);
    //sqlite3IdListDelete(pUsing);
    sqlite3SelectDelete(pSubquery);
    return 0;
}


/*
** Expand the space allocated for the given SrcList object by
** creating nExtra new slots beginning at iStart.  iStart is zero based.
** New slots are zeroed.
**
** For example, suppose a SrcList initially contains two entries: A,B.
** To append 3 new entries onto the end, do this:
**
**    sqlite3SrcListEnlarge(db, pSrclist, 3, 2);
**
** After the call above it would contain:  A, B, nil, nil, nil.
** If the iStart argument had been 1 instead of 2, then the result
** would have been:  A, nil, nil, nil, B.  To prepend the new slots,
** the iStart value would be 0.  The result then would
** be: nil, nil, nil, A, B.
**
** If a memory allocation fails the SrcList is unchanged.  The
** db->mallocFailed flag will be set to true.
*/
SrcList *sqlite3SrcListEnlarge(
  SrcList *pSrc,     /* The SrcList to be enlarged */
  int nExtra,        /* Number of new slots to add to pSrc->a[] */
  int iStart         /* Index in pSrc->a[] of first new slot */
){
  int i;

  /* Sanity checking on calling parameters */
  assert( iStart>=0 );
  assert( nExtra>=1 );
  assert( pSrc!=0 );
  assert( iStart<=pSrc->nSrc );

  /* Allocate additional space if needed */
  if( pSrc->nSrc+nExtra>pSrc->nAlloc ){
    SrcList *pNew;
    int nAlloc = pSrc->nSrc+nExtra;
    //int nGot;
    pNew = (SrcList *)realloc(pSrc,
               sizeof(*pSrc) + (nAlloc-1)*sizeof(pSrc->a[0]) );
    if( pNew==0 ){
      return pSrc;
    }
    pSrc = pNew;
    /*nGot = (sqlite3DbMallocSize(pNew) - sizeof(*pSrc))/sizeof(pSrc->a[0])+1;*/
    pSrc->nAlloc++;
  }

  /* Move existing slots that come after the newly inserted slots
  ** out of the way */
  for(i=pSrc->nSrc-1; i>=iStart; i--){
    pSrc->a[i+nExtra] = pSrc->a[i];
  }
  pSrc->nSrc += (i16)nExtra;

  /* Zero the newly allocated slots */
  memset(&pSrc->a[iStart], 0, sizeof(pSrc->a[0])*nExtra);
  for(i=iStart; i<iStart+nExtra; i++){
    pSrc->a[i].iCursor = -1;
  }

  /* Return a pointer to the enlarged SrcList */
  return pSrc;
}


char *sqlite3DbStrNDup(const char *z, int n){
    char *zNew;
    if( z==0 ){
        return 0;
    }
    assert( (n&0x7fffffff)==n );
    zNew = (char *)malloc(n+1);
    if( zNew ){
        memcpy(zNew, z, n);
        zNew[n] = 0;
    }
    return zNew;
}

/*
** Given a token, return a string that consists of the text of that
** token.  Space to hold the returned string
** is obtained from sqliteMalloc() and must be freed by the calling
** function.
**
** Any quotation marks (ex:  "name", 'name', [name], or `name`) that
** surround the body of the token are removed.
**
** Tokens are often just pointers into the original SQL text and so
** are not \000 terminated and are not persistent.  The returned string
** is \000 terminated and is persistent.
*/
char *sqlite3NameFromToken(Token *pName){
    char *zName;
    if( pName ){
        zName = sqlite3DbStrNDup((char*)pName->z, pName->n);
        sqlite3Dequote(zName);
    }else{
        zName = 0;
    }
    return zName;
}

SrcList *sqlite3SrcListAppend(
    SrcList *pList,     /* Append to this SrcList. NULL creates a new SrcList */
    Token *pTable,      /* Table to append */
    Token *pDatabase    /* Database of the table */
    ){
        SrcList::SrcList_item *pItem;
        assert( pDatabase==0 || pTable!=0 );  /* Cannot have C without B */
        if( pList==0 ){
            pList = (SrcList*)sqlite3DbMallocZero(sizeof(SrcList) );
            if( pList==0 ) return 0;
            pList->nAlloc = 1;
        }
        pList = sqlite3SrcListEnlarge(pList, 1, pList->nSrc);
        pItem = &pList->a[pList->nSrc-1];
        if( pDatabase && pDatabase->z==0 ){
            pDatabase = 0;
        }
        if( pDatabase ){
            Token *pTemp = pDatabase;
            pDatabase = pTable;
            pTable = pTemp;
        }
        pItem->zName = sqlite3NameFromToken(pTable);
        pItem->zDatabase = sqlite3NameFromToken(pDatabase);
        return pList;
}

int sqlite3Select( Parse *pParse, /* The parser context */ Select *p, /* The SELECT statement being coded. */ SelectDest *pDest /* What to do with the query results */ )
{
    return 0;
}

Select * sqlite3SelectNew( Parse *pParse, /* Parsing context */ ExprList *pEList, /* which columns to include in the result */ SrcList *pSrc, /* the FROM clause -- which tables to scan */ Expr *pWhere, /* the WHERE clause */ ExprList *pGroupBy, /* the GROUP BY clause */ Expr *pHaving, /* the HAVING clause */ ExprList *pOrderBy, /* the ORDER BY clause */ u16 selFlags, /* true if the DISTINCT keyword is present */ Expr *pLimit, /* LIMIT value. NULL means not used */ Expr *pOffset /* OFFSET value. NULL means no offset */ )
{
    if (pEList) {
        for (int i = 0; i < pEList->nExpr; ++i) {
            printf("result column %s \n", pEList->a[i].zSpan);
        }
    }

    for (int i = 0; i < pSrc->nSrc; ++i) {
        printf("from table %s \n", pSrc->a[i].zName);
    }

    /*for (int i = 0; i < pWhere->nSrc; ++i) {
        printf("from table %s \n", pSrc->a[0].zName);
    }*/

    Select *pNew;
    Select standin;
    pNew = (Select*)sqlite3DbMallocZero(sizeof(*pNew) );
    if( pNew==0 ){
        pNew = &standin;
        memset(pNew, 0, sizeof(*pNew));
    }
    if( pEList==0 ){
        pEList = sqlite3ExprListAppend(pParse, 0, sqlite3Expr(TK_ALL,0));
    }
    pNew->pEList = pEList;
    if( pSrc==0 ) pSrc = (SrcList*)sqlite3DbMallocZero(sizeof(*pSrc));
    pNew->pSrc = pSrc;
    pNew->pWhere = pWhere;
    pNew->pGroupBy = pGroupBy;
    pNew->pHaving = pHaving;
    pNew->pOrderBy = pOrderBy;
    pNew->selFlags = selFlags;
    pNew->op = TK_SELECT;
    pNew->pLimit = pLimit;
    pNew->pOffset = pOffset;
    assert( pOffset==0 || pLimit!=0 );
    pNew->addrOpenEphm[0] = -1;
    pNew->addrOpenEphm[1] = -1;
    pNew->addrOpenEphm[2] = -1;
    assert( pNew!=&standin );
    return pNew;
    return 0;
}


/*
** If zNum represents an integer that will fit in 32-bits, then set
** *pValue to that integer and return true.  Otherwise return false.
**
** Any non-numeric characters that following zNum are ignored.
** This is different from sqlite3Atoi64() which requires the
** input number to be zero-terminated.
*/
int sqlite3GetInt32(const char *zNum, int *pValue){
    long long int v = 0;
    int i, c;
    int neg = 0;
    if( zNum[0]=='-' ){
        neg = 1;
        zNum++;
    }else if( zNum[0]=='+' ){
        zNum++;
    }
    while( zNum[0]=='0' ) zNum++;
    for(i=0; i<11 && (c = zNum[i] - '0')>=0 && c<=9; i++){
        v = v*10 + c;
    }
    
  /* The longest decimal representation of a 32 bit integer is 10 digits:
  **
  **             1234567890
  **     2^31 -> 2147483648
  */
  testcase( i==10 );
  if( i>10 ){
    return 0;
  }
  testcase( v-neg==2147483647 );
  if( v-neg>2147483647 ){
    return 0;
  }
  if( neg ){
    v = -v;
  }
  *pValue = (int)v;
  return 1;
}
/*
** If the expression p codes a constant integer that is small enough
** to fit in a 32-bit integer, return 1 and put the value of the integer
** in *pValue.  If the expression is not an integer or if it is too big
** to fit in a signed 32-bit integer, return 0 and leave *pValue unchanged.
*/
int sqlite3ExprIsInteger(Expr *p, int *pValue)
{
  int rc = 0;

  /* If an expression is an integer literal that fits in a signed 32-bit
  ** integer, then the EP_IntValue flag will have already been set */
  /*assert( p->op!=TK_INTEGER || (p->flags & EP_IntValue)!=0
           || sqlite3GetInt32(p->u.zToken, &rc)==0 );*/

  if( p->flags & EP_IntValue ){
    *pValue = p->u.iValue;
    return 1;
  }
  switch( p->op ){
    case TK_UPLUS: {
      rc = sqlite3ExprIsInteger(p->pLeft, pValue);
      break;
    }
    case TK_UMINUS: {
      int v;
      if( sqlite3ExprIsInteger(p->pLeft, &v) ){
        *pValue = -v;
        rc = 1;
      }
      break;
    }
    default: break;
  }
  return rc;
}

/*
** Return 1 if an expression must be FALSE in all cases and 0 if the
** expression might be true.  This is an optimization.  If is OK to
** return 0 here even if the expression really is always false (a 
** false negative).  But it is a bug to return 1 if the expression
** might be true in some rare circumstances (a false positive.)
**
** Note that if the expression is part of conditional for a
** LEFT JOIN, then we cannot determine at compile-time whether or not
** is it true or false, so always return 0.
*/
int exprAlwaysFalse(Expr *p){
    int v = 0;
    if( ExprHasProperty(p, EP_FromJoin) ) return 0;
    if( !sqlite3ExprIsInteger(p, &v) ) return 0;
    return v==0;
}


/*
** Convert an SQL-style quoted string into a normal string by removing
** the quote characters.  The conversion is done in-place.  If the
** input does not begin with a quote character, then this routine
** is a no-op.
**
** The input string must be zero-terminated.  A new zero-terminator
** is added to the dequoted string.
**
** The return value is -1 if no dequoting occurs or the length of the
** dequoted string, exclusive of the zero terminator, if dequoting does
** occur.
**
** 2002-Feb-14: This routine is extended to remove MS-Access style
** brackets from around identifers.  For example:  "[a-b-c]" becomes
** "a-b-c".
*/
int sqlite3Dequote(char *z){
    char quote;
    int i, j;
    if( z==0 ) return -1;
    quote = z[0];
    switch( quote ){
    case '\'':  break;
    case '"':   break;
    case '`':   break;                /* For MySQL compatibility */
    case '[':   quote = ']';  break;  /* For MS SqlServer compatibility */
    default:    return -1;
    }
    for(i=1, j=0; ALWAYS(z[i]); i++){
        if( z[i]==quote ){
            if( z[i+1]==quote ){
                z[j++] = quote;
                i++;
            }else{
                break;
            }
        }else{
            z[j++] = z[i];
        }
    }
    z[j] = 0;
    return j;
}

/*
** This routine is the core allocator for Expr nodes.
**
** Construct a new expression node and return a pointer to it.  Memory
** for this node and for the pToken argument is a single allocation
** obtained from sqlite3DbMalloc().  The calling function
** is responsible for making sure the node eventually gets freed.
**
** If dequote is true, then the token (if it exists) is dequoted.
** If dequote is false, no dequoting is performance.  The deQuote
** parameter is ignored if pToken is NULL or if the token does not
** appear to be quoted.  If the quotes were of the form "..." (double-quotes)
** then the EP_DblQuoted flag is set on the expression node.
**
** Special case:  If op==TK_INTEGER and pToken points to a string that
** can be translated into a 32-bit integer, then the token is not
** stored in u.zToken.  Instead, the integer values is written
** into u.iValue and the EP_IntValue flag is set.  No extra storage
** is allocated to hold the integer text and the dequote flag is ignored.
*/
Expr *sqlite3ExprAlloc(
    int op,                 /* Expression opcode */
    const Token *pToken,    /* Token argument.  Might be NULL */
    int dequote             /* True to dequote */
    ){
        Expr *pNew;
        int nExtra = 0;
        int iValue = 0;

        if( pToken ){
            if( op!=TK_INTEGER || pToken->z==0
                || sqlite3GetInt32(pToken->z, &iValue)==0 ){
                    nExtra = pToken->n+1;
            }
        }
        pNew = (Expr *)sqlite3DbMallocZero(sizeof(Expr)+nExtra);
        if( pNew ){
            pNew->op = (u8)op;
            pNew->iAgg = -1;
            if( pToken ){
                if( nExtra==0 ){
                    pNew->flags |= EP_IntValue;
                    pNew->u.iValue = iValue;
                }else{
                    int c;
                    pNew->u.zToken = (char*)&pNew[1];
                    if( pToken->n ) memcpy(pNew->u.zToken, pToken->z, pToken->n);
                    pNew->u.zToken[pToken->n] = 0;
                    if( dequote && nExtra>=3 
                        && ((c = pToken->z[0])=='\'' || c=='"' || c=='[' || c=='`') ){
                            sqlite3Dequote(pNew->u.zToken);
                            if( c=='"' ) pNew->flags |= EP_DblQuoted;
                    }
                }
            }
#if SQLITE_MAX_EXPR_DEPTH>0
            pNew->nHeight = 1;
#endif  
        }
        return pNew;
}
/*
** Join two expressions using an AND operator.  If either expression is
** NULL, then just return the other expression.
**
** If one side or the other of the AND is known to be false, then instead
** of returning an AND expression, just return a constant expression with
** a value of false.
*/
Expr *sqlite3ExprAnd(Expr *pLeft, Expr *pRight){
    if( pLeft==0 ){
        return pRight;
    }else if( pRight==0 ){
        return pLeft;
    }else if( exprAlwaysFalse(pLeft) || exprAlwaysFalse(pRight) ){
        sqlite3ExprDelete(pLeft);
        sqlite3ExprDelete(pRight);
        return sqlite3ExprAlloc(TK_INTEGER, &sqlite3IntTokens[0], 0);
    }else{
        Expr *pNew = sqlite3ExprAlloc(TK_AND, 0, 0);
        sqlite3ExprAttachSubtrees(pNew, pLeft, pRight);
        return pNew;
    }
}



/*
** Attach subtrees pLeft and pRight to the Expr node pRoot.
**
** If pRoot==NULL that means that a memory allocation error has occurred.
** In that case, delete the subtrees pLeft and pRight.
*/
void sqlite3ExprAttachSubtrees(
    Expr *pRoot,
    Expr *pLeft,
    Expr *pRight
    ){
        if( pRoot==0 ){
            sqlite3ExprDelete(pLeft);
            sqlite3ExprDelete(pRight);
        }else{
            if( pRight ){
                pRoot->pRight = pRight;
                pRoot->flags |= EP_Collate & pRight->flags;
            }
            if( pLeft ){
                pRoot->pLeft = pLeft;
                pRoot->flags |= EP_Collate & pLeft->flags;
            }
        }
}

/*
** Allocate a Expr node which joins as many as two subtrees.
**
** One or both of the subtrees can be NULL.  Return a pointer to the new
** Expr node.  Or, if an OOM error occurs, set pParse->db->mallocFailed,
** free the subtrees and return NULL.
*/
Expr *sqlite3PExpr(
    Parse *pParse,          /* Parsing context */
    int op,                 /* Expression opcode */
    Expr *pLeft,            /* Left operand */
    Expr *pRight,           /* Right operand */
    const Token *pToken     /* Argument token */
    ){
        Expr *p;
        if( op==TK_AND && pLeft && pRight ){
            /* Take advantage of short-circuit false optimization for AND */
            p = sqlite3ExprAnd(pLeft, pRight);
        }else{
            p = sqlite3ExprAlloc(op, pToken, 1);
            sqlite3ExprAttachSubtrees(p, pLeft, pRight);
        }
        return p;
}


/*
** When building up a FROM clause in the parser, the join operator
** is initially attached to the left operand.  But the code generator
** expects the join operator to be on the right operand.  This routine
** Shifts all join operators from left to right for an entire FROM
** clause.
**
** Example: Suppose the join is like this:
**
**           A natural cross join B
**
** The operator is "natural cross join".  The A and B operands are stored
** in p->a[0] and p->a[1], respectively.  The parser initially stores the
** operator with A.  This routine shifts that operator over to B.
*/
void sqlite3SrcListShiftJoinType(SrcList *p){
    if( p ){
        int i;
        assert( p->a || p->nSrc==0 );
        for(i=p->nSrc-1; i>0; i--){
            p->a[i].jointype = p->a[i-1].jointype;
        }
        p->a[0].jointype = 0;
    }
}



/*
** Allocate a new expression node from a zero-terminated token that has
** already been dequoted.
*/
Expr *sqlite3Expr(
    int op,                 /* Expression opcode */
    const char *zToken      /* Token argument.  Might be NULL */
    ){
        Token x;
        x.z = zToken;
        x.n = zToken ? sqlite3Strlen30(zToken) : 0;
        return sqlite3ExprAlloc(op, &x, 0);
}


/*
** Compute a string length that is limited to what can be stored in
** lower 30 bits of a 32-bit signed integer.
**
** The value returned will never be negative.  Nor will it ever be greater
** than the actual length of the string.  For very long strings (greater
** than 1GiB) the value returned might be less than the true string length.
*/
int sqlite3Strlen30(const char *z){
    const char *z2 = z;
    if( z==0 ) return 0;
    while( *z2 ){ z2++; }
    return 0x3fffffff & (int)(z2 - z);
}


