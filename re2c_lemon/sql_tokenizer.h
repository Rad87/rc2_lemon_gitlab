#ifndef __SQL_TOKENIZER_H__
#define __SQL_TOKENIZER_H__


#include <vector>

namespace utils
{

    enum token_t
    {
        eTOKEN_INVALID,
        eTOKEN_ID,
        eTOKEN_STRING_SQUOTE,
        eTOKEN_STRING_DQUOTE,
        eTOKEN_PERCENT,
        eTOKEN_AMPERSAND,
        eTOKEN_DOLLAR,
        eTOKEN_LEFT_PAREN,
        eTOKEN_RIGHT_PAREN,
        eTOKEN_ASTERISK,
        eTOKEN_PLUS_SIGN,
        eTOKEN_COMMA,
        eTOKEN_MINUS_SIGN,
        eTOKEN_PERIOD,
        eTOKEN_SOLIDUS,
        eTOKEN_COLON,
        eTOKEN_SEMICOLON,
        eTOKEN_LESS_THAN_OPERATOR,
        eTOKEN_EQUALS_OPERATOR,
        eTOKEN_GREATER_THAN_OPERATOR,
        eTOKEN_QUESTION_MARK,
        eTOKEN_UNDERSCORE,
        eTOKEN_VERTICAL_BAR,
        eTOKEN_LEFT_BRACKET,
        eTOKEN_RIGHT_BRACKET,
        eTOKEN_SHARP,
        eTOKEN_NUMBER
    };

    struct token
    {
        token ();

        void set (token_t type, const char* begin, const char* end);

        const char* text;
        unsigned    len;
        token_t     type;
    };

    class scanner_state
    {
    public:
        enum state_t
        {
            ePARSE,
            eONE_LINE_COMMENT,
            eMULTI_LINE_COMMENT,
            eIN_SQUOTED_STRING,
            eIN_DQUOTED_STRING
        };
    public:
        scanner_state (const char* p, const char* last);

        const char* begin () const;
        void begin (const char* p);

        const char* end () const;

        state_t state () const;
        void state (state_t st);




    private:
        const char* m_start;
        const char* m_last;
        state_t m_state;
    };


    const char* scan (scanner_state& state, token& token);

    typedef std::vector <token> TokensVector;

    
    inline
    bool tokenize_sql (const char* begin, const char* end, TokensVector& tokens)
    {
        const char* p = begin;
        tokens.reserve (512);
        std::size_t k = 0;            
        while (p != end)
        {
            scanner_state st (p, end);
            tokens.resize (k+1);

            token& t = tokens [k];

            p = scan (st, t);
            if (t.type != eTOKEN_INVALID)
            {
                // advance to the next token
                k++;
            }
            else
            {
                if (p != end)
                {
                    if (*p == 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
        }
        return true;
    }
    // =======================================================
    // implementation
    // =======================================================
    inline
        token::token ()
        : text (0),
        len  (0),
        type (eTOKEN_INVALID)
    {
    }
    // -------------------------------------------------------
    inline
        void token::set (token_t tp, const char* begin, const char* end)
    {
        text = begin;
        len = end - begin;
        type = tp;
    }
    // =======================================================
    inline
        scanner_state::scanner_state (const char* p, const char* last)
        : m_start (p),
        m_last  (last),
        m_state (ePARSE)
    {
    }
    // ------------------------------------------------------
    inline
        const char* scanner_state::begin () const
    {
        return m_start;
    }
    // ------------------------------------------------------
    inline
        const char* scanner_state::end () const
    {
        return m_last;
    }
    // ------------------------------------------------------
    inline
        scanner_state::state_t scanner_state::state () const
    {
        return m_state;
    }
    // ------------------------------------------------------
    inline
        void scanner_state::state (state_t st)
    {
        m_state = st;
    }
    // ------------------------------------------------------
    inline
        void scanner_state::begin (const char* p)
    {
        m_start = p;
    }
} // ns utils
#endif
