#include <stdio.h>
#include "sql_tokenizer.h"

namespace utils {

#define YYCTYPE char
#define YYCURSOR p

/*!re2c
  lletter               = [a-z];
  uletter               = [A-Z];
  letter                = lletter | uletter;
  digit                 = [0-9];
  space                 = ' ';
  dquote                = '\"';
  percent               = '%';
  ampersand             = '&';
  dollar                = '$';
  squote                = '\'';
  left_paren            = '(';
  right_paren           = ')';
  asterisk              = '*';
  plus_sign             = '+';
  comma                 = ',';
  minus_sign            = '-';
  period                = '.';
  solidus               = '/';
  colon                 = ':';
  semicolon             = ';';
  less_than_operator    = '<';
  equals_operator       = '=';
  greater_than_operator = '>';
  question_mark         = '?';
  underscore            = '_';
  vertical_bar          = '|';
  left_bracket          = '[';
  right_bracket         = ']';
  sharp                 = '#';

  special_char          = space	| dquote | percent | ampersand | squote | left_paren | right_paren
                                | asterisk | plus_sign | comma | minus_sign | period | solidus 
				| colon | semicolon | less_than_operator | greater_than_operator
				| equals_operator | question_mark | underscore | vertical_bar;

  any = [\000-\377];			       
  ws  = [ \r\t\n];
  identifier = letter (letter | digit | underscore | dollar | sharp )*;
 */

const char* scan(scanner_state& state, token& token)
{
  const char* p = state.begin ();
/*!re2c
        re2c:yyfill:enable   = 0;
        re2c:yych:conversion = 1;
        re2c:indent:top      = 1;
*/
 scan:;
  if (p == state.end ())
    {
      return p;
    }
  state.state (scanner_state::ePARSE);
  state.begin (YYCURSOR);
  /*!re2c

    digit+ {
        token.set (eTOKEN_NUMBER, state.begin (), YYCURSOR);
        return YYCURSOR;
    }

    identifier {
    token.set (eTOKEN_ID, state.begin (), YYCURSOR);
    return YYCURSOR;
    }

    squote {
    state.begin (YYCURSOR);
    state.state (scanner_state::eIN_SQUOTED_STRING); 
    goto string;
    }

    dquote {
    state.begin (YYCURSOR);
    state.state (scanner_state::eIN_DQUOTED_STRING); 
    goto string;
    }

    percent{ token.set (eTOKEN_PERCENT, state.begin (), YYCURSOR); return YYCURSOR; }
    ampersand{ token.set (eTOKEN_AMPERSAND, state.begin (), YYCURSOR); return YYCURSOR; }
    dollar { token.set (eTOKEN_DOLLAR, state.begin (), YYCURSOR); return YYCURSOR; }
    left_paren { token.set (eTOKEN_LEFT_PAREN, state.begin (), YYCURSOR); return YYCURSOR; }
    right_paren { token.set (eTOKEN_RIGHT_PAREN, state.begin (), YYCURSOR); return YYCURSOR; }
    asterisk { token.set (eTOKEN_ASTERISK, state.begin (), YYCURSOR); return YYCURSOR; }
    plus_sign { token.set (eTOKEN_PLUS_SIGN, state.begin (), YYCURSOR); return YYCURSOR; }
    comma { token.set (eTOKEN_COMMA, state.begin (), YYCURSOR); return YYCURSOR; }
    minus_sign { token.set (eTOKEN_MINUS_SIGN, state.begin (), YYCURSOR); return YYCURSOR; }
    period { token.set (eTOKEN_PERIOD, state.begin (), YYCURSOR); return YYCURSOR; }
    solidus { token.set (eTOKEN_SOLIDUS, state.begin (), YYCURSOR); return YYCURSOR; }
    colon { token.set (eTOKEN_COLON, state.begin (), YYCURSOR); return YYCURSOR; }
    semicolon { token.set (eTOKEN_SEMICOLON, state.begin (), YYCURSOR); return YYCURSOR; }
    less_than_operator { token.set (eTOKEN_LESS_THAN_OPERATOR, state.begin (), YYCURSOR); return YYCURSOR; }
    equals_operator { token.set (eTOKEN_EQUALS_OPERATOR, state.begin (), YYCURSOR); return YYCURSOR; }
    greater_than_operator { token.set (eTOKEN_GREATER_THAN_OPERATOR, state.begin (), YYCURSOR); return YYCURSOR; }
    question_mark { token.set (eTOKEN_QUESTION_MARK, state.begin (), YYCURSOR); return YYCURSOR; }
    underscore { token.set (eTOKEN_UNDERSCORE, state.begin (), YYCURSOR); return YYCURSOR; }
    vertical_bar { token.set (eTOKEN_VERTICAL_BAR, state.begin (), YYCURSOR); return YYCURSOR; }
    left_bracket { token.set (eTOKEN_LEFT_BRACKET, state.begin (), YYCURSOR); return YYCURSOR; }
    right_bracket { token.set (eTOKEN_RIGHT_BRACKET, state.begin (), YYCURSOR); return YYCURSOR; }
    sharp { token.set (eTOKEN_SHARP, state.begin (), YYCURSOR); return YYCURSOR; }
    

    ws+ { goto scan; }

    "--" { state.state (scanner_state::eONE_LINE_COMMENT); goto comment; }

    "/*" { state.state (scanner_state::eMULTI_LINE_COMMENT); goto comment; }

    any { return state.begin (); }
   */
 string:
  if (p == state.end ())
    {
      return p;
    }
  /*!re2c
    squote { 
        if (state.state () == scanner_state::eIN_SQUOTED_STRING)
	{
	    token.set (eTOKEN_STRING_SQUOTE, state.begin (), YYCURSOR - 1);
	    return YYCURSOR;
	}
	goto string;
    }
    dquote {
    if (state.state () == scanner_state::eIN_DQUOTED_STRING)
	{
   	   token.set (eTOKEN_STRING_DQUOTE, state.begin (), YYCURSOR - 1);
	   return YYCURSOR;
	}
	goto string;
    }
    any { goto string; }
   */
 comment:
  if (p == state.end ())
    {
      return p;
    }
  /*!re2c
    '\n' {
           if (state.state () == scanner_state::eONE_LINE_COMMENT)
	   {
	      
	      goto scan;
	   }
	   goto comment;
	 }
    "*/" {
          if (state.state () == scanner_state::eMULTI_LINE_COMMENT)
	   {
	      goto scan;
	   }
	   goto comment;
         }
     any { goto comment; }
   */
}

} // ns utils
